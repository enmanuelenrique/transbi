class StaticPagesController < ApplicationController
  def home
    @listings = Listing.last(4)
  end

  def help
  end
  
  def about
  end
  
  def contact
  end
  
end
