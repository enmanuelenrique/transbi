class BankAccountsController < ApplicationController
  
  before_action :logged_in_user, olny: [:index, :new, :create, :destroy]
  before_action :admin_user, only: [:monitoreo, :confirmar]
  
  def index
    @bank_account= BankAccount.where(user: current_user)
  end


  def new
    @bank_account = current_user.bank_accounts.build
    @bancos = ["Popular", "Banreservas", "Progreso", "BHD Leon", "Scotiabank", "Santa Cruz",
                    "Banco Ademi"]
  end
  
  
  def monitoreo
    @bank_accounts = BankAccount.all
  end
  
  def confirmar 
   @bank_account = BankAccount.find(params[:id])
    if @bank_account.update_attributes(status: params[:status])
      flash[:success] = "Cambios guardados"
    else
      flash[:danger] = "Algo pasó intente de nuevo"
    end
   redirect_to bank_accounts_monitoreo_path
  end
  
  def create
    @bank_account = current_user.bank_accounts.build(bank_params)
    
    if @bank_account.save
      flash[:success] = "Solicitud recibida! Te avisaremos cuando confirmemos la Cuentas"
      redirect_to action: "index"
    else
      render 'new'
    end
  end
  
  def destroy
    BankAccount.find(params[:id]).destroy
    flash[:success] = "Cuenta eliminada"
    redirect_to bank_accounts_url
  end

  #def edit
  #  @bank_account = BankAccount.find(params[:id])
  #end

  #def update
  #end
  
  private
  
  def bank_params
    params.require(:bank_account).permit(:titular, :banco, :cuenta)
  end
  
  
end
