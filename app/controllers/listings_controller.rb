class ListingsController < ApplicationController
  before_action :set_listing, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:new, :create, :destroy]
  before_action :correct_user,   only: [:destroy, :edit, :update]
  before_action :complete_user, only: [:new, :create]
  # GET /listings
  # GET /listings.json
  
  def search
    search = params[:search].presence || "*"
    conditions = {}
    conditions[:price] = params[:price] if params[:price].present?
    @listings = Listing.search search, where: conditions, aggs: [:price]
  end
  
  def seller
    @listings = Listing.where(user: current_user).order("created_at DESC")
  end
  
  def index
    if params[:subcategory].blank?
      @listings = Listing.all.order("created_at DESC")
    else
      @subcategory_id = Subcategory.find_by(name: params[:subcategory]).id
      @listings = Listing.where(subcategory_id: @subcategory_id).order("created_at DESC")
    end
  end
  
  def get_subcategories
    @subcategories = Subcategory.where(category_id: params[:category_id])
  end

  # GET /listings/1
  # GET /listings/1.json
  def show
    @comment = @listing.comments.build if logged_in?
  end

  # GET /listings/new
  def new
    @listing = current_user.listings.build
  end

  # GET /listings/1/edit
  def edit
  end

  # POST /listings
  # POST /listings.json
  def create
    @listing = current_user.listings.build(listing_params)

    if @listing.save
      flash[:success] = "listing created!"
      redirect_to @listing
    else
      render 'new'
    end
  end

  # PATCH/PUT /listings/1
  # PATCH/PUT /listings/1.json
  def update
    respond_to do |format|
      if @listing.update(listing_params)
        format.html { redirect_to @listing, notice: 'Listing was successfully updated.' }
        format.json { render :show, status: :ok, location: @listing }
      else
        format.html { render :edit }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /listings/1
  # DELETE /listings/1.json
  def destroy
    @listing.destroy
    respond_to do |format|
      format.html { redirect_to listings_url, notice: 'Listing was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_listing
      @listing = Listing.find(params[:id])
    end
    

    def correct_user
      unless current_user?(@listing.user)
        redirect_to(root_url)
        flash[:danger] = "Sorry, this listing belongs to someone else"
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def listing_params
      params.require(:listing).permit(:name, :description, :price, :picture, 
                                          :subcategory_id, :category_id,
                                          pickup_address_attributes: [:city, :sector,
                                                            :line_1, :line_2])
    end
end
