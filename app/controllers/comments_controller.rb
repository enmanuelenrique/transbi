class CommentsController < ApplicationController
    before_action :logged_in_user, only: [:create, :destroy]
    before_action :correct_user,   only: :destroy
    
    
    def create
         @listing = Listing.find(params[:listing_id])
         @comment = @listing.comments.build(comment_params)
         @comment.user = current_user
         
        if @comment.save
            redirect_to listing_path(@listing)
        else
            render :new
        end
        
    end
    
    def destroy
        @listing = Listing.find(params[:listing_id])
        @comment = @listing.comments.find(params[:id])
        @comment.destroy
        flash[:success] = "Comentario eliminado"
        redirect_to request.referrer || root_url
    end
    
    private
    
    def comment_params
       params.require(:comment).permit(:content) 
    end
    
    def correct_user
        @comment = current_user.comments.find_by(id: params[:id])
        
        redirect_to request.referrer || root_url if @comment.nil?
        flash[:danger] = "Este comentario le pertenece a otra persona"
    end
    
end
