class TransfersController < ApplicationController
  before_action :logged_in_user, only: [:index, :new, :historial, :create]
  before_action :admin_user, only: [:confirmar, :monitoreo]
  
  def new
    @bank = BankAccount.find(params[:bank_account_id])
    if @bank.confirmado?
      @transferencia = @bank.transfers.build
    else
      flash[:danger] = "Cuenta aun no confirmada"
      redirect_to bank_accounts_url
    end
  end

  def index
    @transferencias = Transfer.where(bank_account: current_user.bank_accounts).order("created_at DESC")
  end
  
  def confirmar 
   @transfer = Transfer.find(params[:id])
    if @transfer.update_attributes(status: params[:status])
      flash[:success] = "Cambios guardados"
    else
      flash[:danger] = "Algo pasó intente de nuevo"
    end
   redirect_to transfers_monitoreo_path
  end

  def monitoreo
    #@transferencias = Transfer.where(status: Transfer.statuses[:solicitado]).order("created_at")
    @transferencias = Transfer.all.order("created_at")
  end
  
  def create
    @bank = BankAccount.find(params[:bank_account_id])
    @transferencia = @bank.transfers.build(transfer_params)
    if @transferencia.save
      flash[:success] = "Solicitud recibida! Recibiras el pago de 3 a 7 dias"
      redirect_to bank_accounts_url
    else
      render 'new'
    end
  end
  
  private
  
  def transfer_params
    params.require(:transfer).permit(:monto)
  end
  
end
