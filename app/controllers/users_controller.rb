class UsersController < ApplicationController
  
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
   before_action :admin_user,     only: :destroy
  
  def index
    @users = User.paginate(page: params[:page])
    
  end
  
  def new
    @user = User.new
  end
  
  def show
    @user = User.find(params[:id])
    @listings = Listing.where(user: current_user).order("created_at DESC")
  end
  
  def create
    @user = User.new(new_params)    # Not the final implementation!
    if @user.save
       #@user.send_activation_email
      #flash[:info] = "Please check your email to activate your account."
      flash[:info] = "registrado correctamente"
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
 def update
    @user = User.find(params[:id])
    if @user.update_attributes(update_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
 end
 
  def destroy
   User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  private

    def new_params
      params.require(:user).permit(:email, :password, :name, :apellido, :telefono,
                                   :password_confirmation, :username)
    end
    
    def update_params
      params.require(:user).permit(:name,  :password, :apellido,
                                   :password_confirmation, :telefono,
                                    addresses_attributes: [:city, :sector,
                                    :line_1, :line_2])
    end
    
    
    # Before filters

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
end
