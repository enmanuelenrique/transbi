class OrdersController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :index, :sales, :purchases]
  before_action :complete_user, only: [:new, :create]
  before_action :correct_user,   only: [:new, :create]
  before_action :admin_user, only: [:monitoreo]
  def sales
    @orders = Order.all.where(seller: current_user).order("created_at DESC")
  end
  
  def purchases
    @orders = Order.all.where(buyer: current_user).order("created_at DESC")
  end

  def show
    @listing = Listing.find(params[:id])
    @order 
  end
  
  def monitoreo
    @orders = Order.all.order("status")
  end
  

  def new
    @listing = Listing.find(params[:listing_id])
    @order = @listing.orders.build
    @order.build_delivery_address
  end

  def create
    
    @listing = Listing.find(params[:listing_id])
    @order = @listing.orders.build(order_params)
    @seller = @listing.user
    @order.seller_id = @seller.id
    @order.buyer_id = current_user.id
    
    
    Stripe.api_key = ENV["stripe_api_key"]
    token = params[:stripeToken]

    begin
      charge = Stripe::Charge.create(
        :amount => (@listing.price * 100).floor,
        :currency => "dop",
        :card => token
        )
      flash[:success] = "Thanks for ordering!"
    rescue Stripe::CardError => e
      flash[:danger] = e.message
    end
    
    if @order.save
      @seller.update_attributes(balance: @seller.balance + @listing.price)
      redirect_to listings_path
    else
      render 'new'
    end
  end
  
  def ship
    @order = Order.find(params[:id])
    if @order.update_attributes(status: params[:status])
      flash[:success] = "Producto enviado"
    else
      flash[:danger] = "Algo pasó intente de nuevo"
    end
    redirect_to ordenes_monitoreo_path
  end
  
  
  
  
  private
  
  def order_params
    params.require(:order).permit(delivery_address_attributes: [:city, :sector,
                                                                :line_1, :line_2])
  end
  
  
  def correct_user
      @listing = Listing.find(params[:listing_id])
      if current_user?(@listing.user)
        redirect_to(root_url)
        flash[:danger] = "Sorry, you can't buy your own product"
      end
  end
  
end

  
