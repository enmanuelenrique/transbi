class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  
  private

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
     # Confirms an admin user.
    def admin_user
      unless current_user.admin?
        redirect_to(root_url)
        flash[:danger] = "Acceso denegado"
      end
    end
    
    def complete_user
      unless !current_user.name.nil? && !current_user.apellido.nil? && !current_user.telefono.nil?
        flash[:warning] = "Para poder proceder necesitas llenar las casillas necesarias"
        redirect_to edit_user_path(current_user)
      end
    end
end
