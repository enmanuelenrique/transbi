class Transfer < ApplicationRecord
  belongs_to :bank_account
  enum status: [:solicitado, :en_transito, :realizado, :rechazado]
  validates :monto, numericality: { greater_than: 0 }
  validate :sobregiro
  
  def sobregiro
    if monto > bank_account.user.balance
      errors.add(:monto, "No tiene balance suficiente")
    end
  end
  
end
