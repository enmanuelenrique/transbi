class Listing < ApplicationRecord
  belongs_to :user
  mount_uploader :picture, PictureUploader
  validates :name, :description, :price, :user_id, :category_id, :subcategory_id, presence: true
  validates_presence_of :picture
  validates :price, numericality: { greater_than: 0 }
  validate  :picture_size
  has_many :orders
  has_many :comments
  belongs_to :subcategory
  belongs_to :category
  
  has_one :pickup_address, as: :addressable, class_name: "Address", dependent: :destroy
  
  accepts_nested_attributes_for :pickup_address
  
  searchkick
  
  private
    
    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
  
end
