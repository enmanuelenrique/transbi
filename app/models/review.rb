class Review < ApplicationRecord
  validates :seller_id, :buyer_id, :rating, presence: true  
   
  belongs_to :autor, class_name: "User"
  belongs_to :evaluado, class_name: "User"
end
