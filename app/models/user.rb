class User < ApplicationRecord
    
    has_many :listings, dependent: :destroy
    has_many :comments
    attr_accessor :remember_token, :activation_token, :reset_token
    before_save :downcase_email
    before_create :create_activation_digest
    
    validates :name, length: { maximum: 40}
    validates :apellido, length: { maximum: 40}
    VALID_USERNAME_REGEX = /\A[a-zA-Z0-9\-_]+\z/i
    #validates :username, presence: true, length: { maximum: 20},
    #                            format: {with: VALID_USERNAME_REGEX},
    #                            uniqueness: {case_sensitive: false}
                                
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    validates :email, presence: true, length: {maximum: 255},
                              format: {with: VALID_EMAIL_REGEX},
                              uniqueness: { case_sensitive: false}
                              
    #VALID_TELEFONO_REGEX = /\(?\d{3}\)?[- ]?\d{3}[- ]?\d{4}\z/
    #validates :telefono, length: {maximum: 15}, format: {with: VALID_TELEFONO_REGEX}
    
    validates :balance, numericality: { greater_than_or_equal_to: 0 }

    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
    
    has_many :sales, class_name: "Order", foreign_key: "seller_id"
    has_many :purchases, class_name: "Order", foreign_key: "buyer_id"
    has_many :addresses, as: :addressable, dependent: :destroy
    has_many :bank_accounts, dependent: :destroy
    has_many :reviews
    
    accepts_nested_attributes_for :addresses
    # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  
  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  # Activates an account.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
  private
  
  # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end

end
