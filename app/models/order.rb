class Order < ApplicationRecord
  validates :seller_id, :buyer_id, :listing_id, presence: true  
  
  belongs_to :listing
  belongs_to :buyer, class_name: "User"
  belongs_to :seller, class_name: "User"
  
  has_one :delivery_address, as: :addressable, class_name: "Address", dependent: :destroy
  
  accepts_nested_attributes_for :delivery_address

  enum status: [ :ordered, :shipped, :delivered, :rated ]
  
end
