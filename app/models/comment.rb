class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :listing
  validates :user_id, :listing_id, presence: true
  validates :content, presence: true
end
