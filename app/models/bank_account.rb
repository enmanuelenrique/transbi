class BankAccount < ApplicationRecord
  belongs_to :user
  enum status: [:revision ,:confirmado, :rechazado]
  has_many :transfers
  validates :user_id, :banco, :cuenta, :titular, presence: true
end
