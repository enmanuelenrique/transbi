module FormHelper
  def setup_user(user)
    user.addresses.build if user.addresses.empty?
    user
  end
  
  def setup_listing(listing)
    listing.build_pickup_address if listing.pickup_address.nil?
    listing
  end
  
  #def setup_order(order)
  #  order.build_deliveryaddress if order.deliveryaddress.nil?
  #  order
  #end
  
end