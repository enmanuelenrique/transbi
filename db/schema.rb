# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180215225900) do

  create_table "addresses", force: :cascade do |t|
    t.string   "city"
    t.string   "sector"
    t.string   "line_1"
    t.string   "line_2"
    t.integer  "addressable_id"
    t.string   "addressable_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string   "banco"
    t.string   "cuenta"
    t.string   "titular"
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "status",     default: 0
    t.index ["user_id"], name: "index_bank_accounts_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "listing_id"
    t.index ["listing_id"], name: "index_comments_on_listing_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "listings", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "picture"
    t.integer  "category_id"
    t.integer  "subcategory_id"
    t.index ["subcategory_id"], name: "index_listings_on_subcategory_id"
    t.index ["user_id"], name: "index_listings_on_user_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "listing_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "buyer_id"
    t.integer  "seller_id"
    t.integer  "status",     default: 0
    t.index ["listing_id"], name: "index_orders_on_listing_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.text     "comment"
    t.integer  "rating"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "order_id"
    t.index ["order_id"], name: "index_reviews_on_order_id"
  end

  create_table "subcategories", force: :cascade do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["category_id"], name: "index_subcategories_on_category_id"
  end

  create_table "transfers", force: :cascade do |t|
    t.decimal  "monto"
    t.integer  "bank_account_id"
    t.integer  "status",          default: 0
    t.text     "comentario"
    t.datetime "fecha"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["bank_account_id"], name: "index_transfers_on_bank_account_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: true
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "username"
    t.string   "apellido"
    t.string   "telefono"
    t.decimal  "balance",           default: "0.0"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
