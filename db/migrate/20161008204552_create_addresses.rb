class CreateAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :addresses do |t|
      t.string :city
      t.string :sector
      t.string :line_1
      t.string :line_2
      t.integer :addressable_id
      t.string :addressable_type

      t.timestamps
    end
  end
end
