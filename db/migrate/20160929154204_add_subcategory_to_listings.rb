class AddSubcategoryToListings < ActiveRecord::Migration[5.0]
  def change
    add_reference :listings, :subcategory, foreign_key: true
  end
end
