class RemoveAddressesFromOrders < ActiveRecord::Migration[5.0]
  def change
    remove_column :orders, :address, :string
    remove_column :orders, :city, :string
  end
end
