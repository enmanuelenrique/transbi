class CreateBankAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :bank_accounts do |t|
      t.string :banco
      t.string :cuenta
      t.string :titular
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
