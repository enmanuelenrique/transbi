class CreateListings < ActiveRecord::Migration[5.0]
  def change
    create_table :listings do |t|
      t.string :name
      t.text :description
      t.decimal :price
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
