class AddStatusToBankAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :bank_accounts, :status, :integer, default: 0
  end
end
