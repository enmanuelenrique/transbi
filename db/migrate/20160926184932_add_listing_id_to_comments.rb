class AddListingIdToComments < ActiveRecord::Migration[5.0]
  def change
    add_reference :comments, :listing, foreign_key: true
  end
end
