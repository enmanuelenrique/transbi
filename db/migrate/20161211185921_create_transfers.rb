class CreateTransfers < ActiveRecord::Migration[5.0]
  def change
    create_table :transfers do |t|
      t.decimal :monto
      t.references :bank_account, foreign_key: true
      t.integer :status
      t.text :comentario
      t.datetime :fecha

      t.timestamps
    end
  end
end
