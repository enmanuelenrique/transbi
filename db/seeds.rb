# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name:  "Example User",
             username: "exampleuser",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               username: "user#{n+1}",
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

Category.create!(name: "Electronicos")
Category.create!(name: "Hombres")
Category.create!(name: "Mujeres")
Category.create!(name: "Niños")

Subcategory.create!(name: "Celulares", category_id: 1)
Subcategory.create!(name: "Computadores", category_id: 1)
Subcategory.create!(name: "Tabletas", category_id: 1)


Subcategory.create!(name: "Camisas", category_id: 2)
Subcategory.create!(name: "Zapatos", category_id: 2)
Subcategory.create!(name: "Pantalones", category_id: 2)

Subcategory.create!(name: "Vestidos", category_id: 3)
Subcategory.create!(name: "Blusas", category_id: 3)
Subcategory.create!(name: "Zapatos", category_id: 3)

Subcategory.create!(name: "Abrigos", category_id: 4)
Subcategory.create!(name: "Overall", category_id: 4)
Subcategory.create!(name: "Zapatos", category_id: 4)

i = 1

(1..6).each do |n|
   
   name = Faker::Commerce.product_name
   description = Faker::Lorem.sentence(5)
   price = Faker::Commerce.price
   listing = Listing.create!(name: name,
                    description: description,
                    price: price,
                    picture: File.open(Rails.root + "public/uploads/seed/default#{n}.jpg"),
                    user_id: n, category_id:1, subcategory_id: i)
    city = Faker::Address.city
    sector = Faker::Address.state
    line_1 = Faker::Address.street_address
    line_2 = Faker::Address.secondary_address
    listing.create_pickup_address!(city: city, sector: sector, line_1: line_1, line_2: line_2)
    if n % 2 == 0
      i += 1
    end
end

2.times do |n|
  order = Order.create!(listing_id: n+1, buyer_id: n+1, seller_id: n+2, status: "ordered")
  city = Faker::Address.city
  sector = Faker::Address.state
  line_1 = Faker::Address.street_address
  line_2 = Faker::Address.secondary_address
  order.create_delivery_address!(city: city, sector: sector, line_1: line_1, line_2: line_2)
end

