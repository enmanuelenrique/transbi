Rails.application.routes.draw do
  
  

  get 'reviews/new'

  get 'reviews/create'

  get 'reviews/update'

  root 'static_pages#home'

  resources :listings do
    collection do
      get 'search'
    end
    resources :orders, only: [:show, :new, :create]
    resources :comments, only: [:create, :destroy]
  end
  
  resources :orders, only: [] do
    put "ship", on: :member
  end
  
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]  
  resources :bank_accounts, only: [:new, :create, :index, :destroy] do
    resources :transfers, only: [:new, :create]
  end
  
  resources :bank_accounts, only: [] do
    put "confirmar", on: :member
  end
  
  resources :transfers, only: [] do
    put "confirmar", on: :member
  end
  
  resources :transfers, only: [:index]
  
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'password_reset/new'

  get 'password_reset/edit'
  
  get 'sales', to: "orders#sales"
  get 'purchases', to: "orders#purchases"
  get 'seller', to: "listings#seller"
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get 'bank_accounts/monitoreo', to: 'bank_accounts#monitoreo'
  get 'orders/monitoreo'
  get 'transfers/monitoreo'
  
end
