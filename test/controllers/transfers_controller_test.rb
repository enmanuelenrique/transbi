require 'test_helper'

class TransfersControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get transfers_new_url
    assert_response :success
  end

  test "should get historial" do
    get transfers_historial_url
    assert_response :success
  end

  test "should get pendientes" do
    get transfers_pendientes_url
    assert_response :success
  end

end
