require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:michael)
    @other = users(:archer)
    @listing = listings(:iphone)
    @listing.picture = fixture_file_upload('/rails.png', 'image/png')
    # This code is not idiomatically correct.
    @order = @listing.orders.build(address: "Lorem ipsum", city: "santiago", seller_id: @user.id, buyer_id: @other.id)
  end
  
  test "should be valid" do
    assert @order.valid?
  end
end
