require 'test_helper'

class OrdersPlacementTest < ActionDispatch::IntegrationTest
  
  def setup
    @seller = users(:michael)
    @buyer = users(:archer)
    @listing = listings(:iphone)
    @listing.picture = fixture_file_upload('/rails.png', 'image/png')
  end
  
  test "place order" do
    log_in_as @buyer
    get new_listing_order_path(@listing)
    assert_response :success
    assert_difference 'Order.count', 1 do
      post listing_orders_path, order: { address: "calle 10", city: "santiago"}
    end
  end
  
  test "Seller cant place order" do
    log_in_as @seller
    get new_listing_order_path(@listing)
    assert_redirected_to root_url 
    
  end
end
